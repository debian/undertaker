undertaker (1.6.1-4) UNRELEASED; urgency=medium

  * Migrate repository from alioth to salsa.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 27 May 2022 20:52:47 +0100

undertaker (1.6.1-3) unstable; urgency=medium

  * Rebuild against aspectc++/libpuma 2.0
  * Rebuild against flex 2.6.1 (Closes: #833780), run flex at build-time.

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 05 Mar 2016 18:52:29 -0500

undertaker (1.6.1-2) unstable; urgency=medium

  * Regenerate with Flex 2.6.0 (Closes: #813264)
  * Fix FTBFS with newer Puma/AspectC++
  * Bump standards version (no changes needed)

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 27 Feb 2016 07:52:52 -0500

undertaker (1.6.1-1) unstable; urgency=medium

  * New upstream release
    - Incorporated patch for undertaker-checkpatch (Closes: #769137)
  * Add patch to fix FTBFS 'buildString' is not a member of 'Puma::StrCol'
    (Closes: #797896)
  * Relax CFLAGS (Don't add -Werror to everything)

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 19 Jan 2016 06:15:12 -0500

undertaker (1.6-2) unstable; urgency=medium

  * Use system picsoat >= 960 (Closes: #766273)
  * Apply logic bugfix from upstream for undertaker-checkpatch when
    calucating statistics about Kconfig changes.

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 11 Nov 2014 23:05:38 -0500

undertaker (1.6-1) unstable; urgency=medium

  * New upstream release
  * Drop undertaker-el package. Sorry, I don't have enough time and expertise
    to maintain this integration. Co-Maintainers welcome. Emacs users please
    setup this package in your personal .emacs startup files.
    (Closes: #724535, 724536)

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 19 Oct 2014 12:26:18 -0400

undertaker (1.5-1) unstable; urgency=low

  * New upstream release
  * update emacs dependency (Closes: #754029)
  * Bump standards version (no changes needed)

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 29 Sep 2014 22:19:38 -0400

undertaker (1.4-1) experimental; urgency=low

  * New upstream release
  * Improve package description. Thanks to Justin B Rye <jbr@edlug.org.uk>
    for the great suggestions! Closes: #696159
  * Bump debhelper compat level to 9

 -- Reinhard Tartler <siretart@tauware.de>  Tue, 18 Dec 2012 13:35:31 +0100

undertaker (1.3b-1) unstable; urgency=low

  [ Christoph Egger ]
  * drop patches

  [ Reinhard Tartler ]
  * Tighten build dependency on libpuma

 -- Reinhard Tartler <siretart@tauware.de>  Sat, 30 Jun 2012 12:10:46 +0200

undertaker (1.2-1) unstable; urgency=low

  * New upstream release
    * Drop patch again (Merged upstream)

 -- Christoph Egger <christoph@debian.org>  Thu, 25 Aug 2011 11:25:07 +0200

undertaker (1.1-2) unstable; urgency=low

  * Apply patch from Konstantinos Margaritis for building against newer
    boost (Closes: #625007)

 -- Christoph Egger <christoph@debian.org>  Sat, 25 Jun 2011 16:04:36 +0200

undertaker (1.1-1) unstable; urgency=low

  * New upstream Version
  * Change siretart's E-Mail address
  * Fix copyright information
  * Add Vcs-* Headers

 -- Christoph Egger <christoph@debian.org>  Thu, 10 Feb 2011 22:26:20 +0100

undertaker (1.0-1) unstable; urgency=low

  * Initial release (Closes: #611178)

 -- Christoph Egger <christoph@debian.org>  Wed, 12 Jan 2011 14:51:00 +0100
